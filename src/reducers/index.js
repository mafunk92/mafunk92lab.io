import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';
import ErrorReducer from './ErrorReducer';


export default combineReducers({
  form: formReducer,
  errors: ErrorReducer
});
