import React, { Component } from 'react';


class Home extends Component {

  render() {
    return (
    
      <div className="jumbotron" style={{height: "2000px"}}>
      
        <h1 className="title">Michael Funk</h1>

        <div className="projects-container"></div>

        <div className="additional-container"></div>

      </div>
    );
  }
  
}

export default Home;
