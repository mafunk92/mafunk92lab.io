import React, { Component } from 'react';


class Footer extends Component {

  render() {
    return (

      <div className="footer">
       <a href="http://www.linkedin.com/in/michaelafunk" target="_blank"><i className="fa fa-linkedin" /></a>
       <a href="https://gitlab.com/mafunk92" target="_blank"><i className="fa fa-gitlab" /></a>
      </div>

    );
  }
}

export default Footer;
