import React from 'react';
import Header from './Header';
import Footer from './Footer';


const App = ({ children }) => {
  return (
    <div className="home">
      <Header />
      {children}
      <Footer />
    </div>
  );
};

export default App;