import React, { Component } from 'react';
import { Link } from 'react-router';


class Header extends Component {
  constructor(props) {
    super(props);

    this.state = {  };
  }

  render() {
    return (

      <nav>
        <div className="align-left">
          <div className="heading">
            <Link to={'/'}>Michael Funk</Link>
          </div>
        </div>

        <div className="align-right">
          <Link>Projects</Link>
          <Link  to={'/about'}>About</Link>
        </div>
      </nav>

    );
  }
}

export default Header;
